import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class SingUpTestNegative extends BaseTest {

//    WebDriver driver;
//    @BeforeMethod
//    public void beforeMethod() {
//        driver = Configuration.getDriver();
//    }
//    @AfterMethod
//    public void afterMethod() {
//        driver.quit();
//    }

    @Test
    public void firstNegativeTest() {
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.get("http://www.kurs-selenium.pl/demo/");
        driver.findElements(By.xpath("//li[@id='li_myaccount']")) //są zwracane dwa identyczne elementy wiec nie da sie odwołac do pojedynczego findElement
                .stream()
                .filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);

        driver.findElements(By.xpath("//a[@class='go-text-right' and text()='  Sign Up']")) //też są zwaracne dwa elementy
                .stream()
                .filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);
        //driver.findElements(By.xpath("//a[@class='go-text-right' and text()='  Sign Up']")).get(1).click(); //alternatywne podejście ale nie jest one zawsze skuteczne bo może próbować klikać w niewidoczny element

        //Nowe okno SignUp

        //submit równiez występuje 2x (jeden widoczny, a drugi nie)
        driver.findElements(By.xpath("//div//button[@type='submit']")).stream().filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);

        WebElement infoLackOfEmail = driver.findElement(By.xpath("//div[@class='alert alert-danger']//p[text()='The Email field is required.']"));
        WebElement infoLackOfPassword = driver.findElement(By.xpath("//div[@class='alert alert-danger']//p[text()='The Password field is required.']"));
        WebElement infoLackOfPassword2 = driver.findElement(By.xpath("//div[@class='alert alert-danger']//p[text()='The Password field is required.']"));
        WebElement infoLackOfFirstName = driver.findElement(By.xpath("//div[@class='alert alert-danger']//p[text()='The First name field is required.']"));
        WebElement infoLackOfLastName = driver.findElement(By.xpath("//div[@class='alert alert-danger']//p[text()='The Last Name field is required.']"));

        Assert.assertTrue(infoLackOfEmail.isDisplayed());
        Assert.assertEquals(infoLackOfEmail.getText(),"The Email field is required.");

        Assert.assertTrue(infoLackOfPassword.isDisplayed());
        Assert.assertEquals(infoLackOfPassword.getText(),"The Password field is required.");

        Assert.assertTrue(infoLackOfPassword2.isDisplayed());
        Assert.assertEquals(infoLackOfPassword2.getText(),"The Password field is required.");

        Assert.assertTrue(infoLackOfFirstName.isDisplayed());
        Assert.assertEquals(infoLackOfFirstName.getText(),"The First name field is required.");

        Assert.assertTrue(infoLackOfLastName.isDisplayed());
        Assert.assertEquals(infoLackOfLastName.getText(),"The Last Name field is required.");



        //Drugi sposób:
        List<String> listOfErrors = driver.findElements(By.xpath("//div[@class='alert alert-danger']//p"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());

        Assert.assertTrue(listOfErrors.contains("The Email field is required."));
        Assert.assertTrue(listOfErrors.contains("The Password field is required."));
        Assert.assertTrue(listOfErrors.contains("The Password field is required."));
        Assert.assertTrue(listOfErrors.contains("The First name field is required."));
        Assert.assertTrue(listOfErrors.contains("The Last Name field is required."));


    }

}
