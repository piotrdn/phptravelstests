import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class MainPage extends BaseTest{

//    WebDriver driver;
//    @BeforeTest
//    public void beforetest() {
//        driver = Configuration.getDriver();
//    }
//    @AfterTest
//    public void afterTest() {
//        driver.quit();
//    }

    @Test
    public void firstTest() throws InterruptedException {
        //driver.get("http://www.kurs-selenium.pl/demo/");

        WebElement searchBar = driver.findElement(By.xpath("//span[text()='Search by Hotel or City Name']"));
        searchBar.click();

        WebElement emptySearchBar = driver.findElement(By.xpath("//div[@id='select2-drop']//input"));
        emptySearchBar.sendKeys("Dubai");

        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement subBar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='select2-match' and text()='Dubai']")));
        subBar.click();

        WebElement checkIn = driver.findElement(By.xpath("//input[@type='text' and @placeholder='Check in' and @value and @name ='checkin']"));
        //checkIn.sendKeys("06/08/2023");
        checkIn.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //opcja wyszukiwania checkIn na podstawie kalendarza...
        WebElement callendarCheckIn = driver.findElement(By.xpath("//td[@class='day ' and text()='28']"));
        callendarCheckIn.click();

        WebElement checkOut = driver.findElement(By.xpath("//input[@type='text' and @placeholder='Check out' and @Value and @name='checkout']"));
        checkOut.clear();
        checkOut.sendKeys("30/03/2023");




        WebElement adultChild = driver.findElement(By.id("travellersInput"));
        adultChild.click();

        WebElement adult = driver.findElement(By.id("adultInput"));
        WebElement adult2 = driver.findElement(By.cssSelector("input[id='adultInput']")); //w ten sposódb da się wprowadzić wartość
        adult2.clear();
        adult2.sendKeys("2");
        WebElement child = driver.findElement(By.id("childInput"));
        WebElement child2 = driver.findElement(By.cssSelector("input[id='childInput']"));
        child2.clear();
        child.sendKeys("3");

        WebElement adultPlsButton = driver.findElement(By.id("adultPlusBtn"));
        adultPlsButton.click();

        WebElement buttonSearch = driver.findElement(By.xpath("//button[@type='submit' and @class='btn btn-lg btn-block btn-primary pfb0 loader']"));
        buttonSearch.click();

        //nowe okno: http://www.kurs-selenium.pl/demo/hotels/search/united-arab-emirates/dubai/19-02-2023/25-02-2023/3/3
        //lokalizacja listy hoteli

        //pobranie listy elementów v1:
        List<WebElement> allHotels = driver.findElements(By.xpath("//h4//a//b"));
        System.out.println("Pobranie listy elementów v1: ");
        for(WebElement option: allHotels) {
            System.out.println(option.getText());
        }
        System.out.println("Liczba hoteli: " + allHotels.size());

        //pobranie listy elementów v2:
        List<String> hotelNames = driver.findElements(By.xpath("//h4//a//b")).stream()
                //.map(el -> el.getText()) //getText nie pobierze wzystkich (pobierze tylko te widoczne)
                .map(el -> el.getAttribute("textContent"))
                .collect(Collectors.toList());
        System.out.println("Pobranie listy elementów v2: ");
        //System.out.println(hotelNames);
        hotelNames.forEach(el -> System.out.println(el));
        System.out.println("Liczba hoteli: " + hotelNames.size());

        String hotel1 = "Jumeirah Beach Hotel";
        String hotel2 = "Oasis Beach Tower";
        String hotel3 = "Rose Rayhaan Rotana";
        String hotel4 = "Hyatt Regency Perth";
        Assert.assertEquals(hotelNames.get(0), hotel1);
        Assert.assertEquals(hotelNames.get(1), hotel2);
        Assert.assertEquals(hotelNames.get(2), hotel3);
        Assert.assertEquals(hotelNames.get(3), hotel4);


    }

}
