import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Configuration {

    private static WebDriver driver;

//    public static WebDriver getDriver() { //nie przechodza wszystkie testy uruchamiane jednocześnie
//        if(driver == null) {
//            WebDriverManager.chromedriver().setup();
//            driver = new ChromeDriver();
//        }
//        return driver;
//    }

    public static WebDriver getDriver() {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        return driver;
    }

}
