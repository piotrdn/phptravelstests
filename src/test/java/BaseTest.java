import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    public WebDriver driver;
    @BeforeMethod
    public void beforeMethod() {
        //WebDriverManager.chromedriver().setup();
        //driver = new ChromeDriver();

        driver = Configuration.getDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://www.kurs-selenium.pl/demo/");
    }
//    @AfterMethod
//    public void afterMethod() {
//        driver.quit();
//    }


}
