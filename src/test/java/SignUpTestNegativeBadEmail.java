import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class SignUpTestNegativeBadEmail extends BaseTest {

//    WebDriver driver;
//    @BeforeMethod
//    public void beforeMethod() {
//        driver = Configuration.getDriver();
//    }
//    @AfterMethod
//    public void afterMethod() {
//        driver.quit();
//    }

    @Test
    public void firstTest() {
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.get("http://www.kurs-selenium.pl/demo/");
        driver.findElements(By.xpath("//li[@id='li_myaccount']")) //są zwracane dwa identyczne elementy wiec nie da sie odwołac do pojedynczego findElement
                .stream()
                .filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);

        driver.findElements(By.xpath("//a[@class='go-text-right' and text()='  Sign Up']")) //też są zwaracne dwa elementy
                .stream()
                .filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);
        //driver.findElements(By.xpath("//a[@class='go-text-right' and text()='  Sign Up']")).get(1).click(); //alternatywne podejście ale nie jest one zawsze skuteczne bo może próbować klikać w niewidoczny element

        //Nowe okno SignUp
        WebElement firstName = driver.findElement(By.name("firstname"));
        firstName.click();
        firstName.clear();
        String firstNameStr = "Tomasz";
        firstName.sendKeys(firstNameStr);

        WebElement lastName = driver.findElement(By.name("lastname"));
        lastName.click();
        lastName.clear();
        lastName.sendKeys("ProblemXD");

        WebElement mobileNumber = driver.findElement(By.name("phone"));
        mobileNumber.click();
        mobileNumber.clear();
        mobileNumber.sendKeys("123456789");

        WebElement email = driver.findElement(By.name("email"));
        email.click();
        email.clear();
        String testEmail = "badEmailTest";
        email.sendKeys(testEmail);

        WebElement password = driver.findElement(By.name("password"));
        password.click();
        password.clear();
        String passwordString = "superhaslo";
        password.sendKeys(passwordString);

        WebElement confirmPassword = driver.findElement(By.name("confirmpassword"));
        confirmPassword.click();
        confirmPassword.clear();
        confirmPassword.sendKeys(passwordString);

        //submit równiez występuje 2x (jeden widoczny, a drugi nie)
        driver.findElements(By.xpath("//div//button[@type='submit']")).stream().filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);


        WebElement infoBadEmailValidation = driver.findElement(By.xpath("//div[@class='alert alert-danger']//p[text()='The Email field must contain a valid email address.']"));
        Assert.assertTrue(infoBadEmailValidation.isDisplayed());
        Assert.assertEquals(infoBadEmailValidation.getText(),"The Email field must contain a valid email address.");

    }
}
