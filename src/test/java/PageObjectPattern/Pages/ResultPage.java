package PageObjectPattern.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class ResultPage {

    @FindBy(xpath = "//h4//a//b")
    private List<WebElement> hotelNames;

    @FindBy(xpath = "//h2[text()='No Results Found']")
    private WebElement resultHeading;


    //SignUpTestNegative lack of all parameters test
    @FindBy(xpath = "//div[@class='alert alert-danger']//p[text()='The Email field is required.']")
    private WebElement infoLackOfEmail;
    @FindBy(xpath = "//div[@class='alert alert-danger']//p[text()='The Password field is required.']")
    private WebElement infoLackOfPassword;
    @FindBy(xpath = "//div[@class='alert alert-danger']//p[text()='The Password field is required.']")
    private WebElement infoLackOfPassword2;
    @FindBy(xpath = "//div[@class='alert alert-danger']//p[text()='The First name field is required.']")
    private WebElement infoLackOfFirstName;
    @FindBy(xpath = "//div[@class='alert alert-danger']//p[text()='The Last Name field is required.']")
    private WebElement infoLackOfLastName;



    public ResultPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


    public List<String> getHotelNames() {
        return hotelNames.stream()
                //.map(el -> el.getText()) //getText nie pobierze wszystkich (pobierze tylko te widoczne)
                .map(el -> el.getAttribute("textContent"))
                .collect(Collectors.toList());
    }

    public WebElement getResultHeading() {
        return resultHeading;
    }



    //SignUpTestNegative lack of all parameters test
    public WebElement getInfoLackOfEmail() {
        return infoLackOfEmail;
    }
    public WebElement getInfoLackOfPassword() {
        return infoLackOfPassword;
    }
    public WebElement getInfoLackOfPassword2() {
        return infoLackOfPassword2;
    }
    public WebElement getInfoLackOfFirstName() {
        return infoLackOfFirstName;
    }
    public WebElement getInfoLackOfLastName() {
        return infoLackOfLastName;
    }



}
