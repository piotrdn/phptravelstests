package PageObjectPattern.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

public class SignUpPage {

    @FindBy(xpath = "//li[@id='li_myaccount']") //są zwracane dwa identyczne elementy wiec nie da sie odwołac do pojedynczego findElement
    private List<WebElement> myAccountButton;

    @FindBy(xpath = "//a[@class='go-text-right' and text()='  Sign Up']") //są zwracane dwa identyczne elementy wiec nie da sie odwołac do pojedynczego findElement
    private List<WebElement> signUpButton;
//dwa powyższe elementy sa w tej klasie SignUpPage (a nie tam gdzie rzeczywiscie sa czyli HotelSearchPage)  ponieważ sa one potrzebne do wejścia do nowego okna SignUpPage
    @FindBy(name = "firstname")
    private WebElement firstName;

    @FindBy(name = "lastname")
    private WebElement lastName;

    @FindBy(name = "phone")
    private WebElement mobileNumber;

    @FindBy(name = "email")
    private WebElement email;

    @FindBy(name = "password")
    private WebElement password;

    @FindBy(name = "confirmpassword")
    private WebElement confirmpassword;

    @FindBy(xpath = "//div//button[@type='submit']")
    private List<WebElement> submitButton;

    public SignUpPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }



    public void getVisibleMyAccountButton() {
        myAccountButton.stream()
                .filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);
    }
    public void getSignUpButton() {
        signUpButton.stream()
                .filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);
    }
    public void getVisibleSubmitButton() {
        submitButton.stream().filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);
    }


    public void sendFirstName(String name) {
        firstName.click();
        firstName.clear();
        firstName.sendKeys(name);
    }
    public void sendLastName(String lastname) {
        lastName.click();
        lastName.clear();
        lastName.sendKeys(lastname);
    }
    public void sendMobileNumber(String mobnr) {
        mobileNumber.click();
        mobileNumber.clear();
        mobileNumber.sendKeys(mobnr);
    }
    public void sendEmail() {
        email.click();
        email.clear();
        String testEmail = "tomaszproblemXD"+ randomNumberForEmail() + "@gmail.com";
        email.sendKeys(testEmail); //email jest zapisywany w bazie więc żeby test zawsze przechodził należy generowac unikalne emaile
    }
    public void sendPassword(String pass) {
        password.click();
        password.clear();
        password.sendKeys(pass);
    }
    public void sendconfirmassword(String confirmedPass) {
        confirmpassword.click();
        confirmpassword.clear();
        confirmpassword.sendKeys(confirmedPass);
    }




    public String randomNumberForEmail() {
        Random random = new Random();
        int temp = random.nextInt(1001); //zakres (0-1000)
        String result = Integer.toString(temp);
        return result;
    }

}
