package PageObjectPattern.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class HotelSearchPage {

    @FindBy(xpath = "//span[text()='Search by Hotel or City Name']")
    private WebElement searchBar; //Domyślnie te wszsytkie elementy mają wartość null więc trzeba je zainicjalisować. Do tego używamy PageFactory (inicjalizuje elementy na stronie)

    @FindBy(xpath = "//div[@id='select2-drop']//input")
    private WebElement emptySearchBar;


//    @FindBy(xpath = "//span[@class='select2-match' and text()='Dubai']")
//    private WebElement subBar;


    @FindBy(xpath = "//input[@type='text' and @placeholder='Check in' and @value and @name ='checkin']")
    private WebElement checkIn;

    @FindBy(xpath = "//input[@type='text' and @placeholder='Check out' and @Value and @name='checkout']")
    private WebElement checkOut;

    @FindBy(id = "travellersInput")
    private WebElement adultChild;

    @FindBy(css = "input[id='adultInput']")
    private WebElement adult;

    @FindBy(css = "input[id='childInput']")
    private WebElement child;

    @FindBy(id = "adultPlusBtn")
    private WebElement adultPlsButton;

    @FindBy(xpath = "//button[@type='submit' and @class='btn btn-lg btn-block btn-primary pfb0 loader']")
    private WebElement buttonSearch;

    private WebDriver driver;

    public HotelSearchPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }


    public void setCity(String city) {
        searchBar.click();
        emptySearchBar.sendKeys(city);
        String cityForXpath = city;
        String xpath = "//span[@class='select2-match' and text()='" + cityForXpath + "']";
        driver.findElement(By.xpath(xpath)).click();
        //subBar.click();
    }

    public void setDates(String checkInStr, String checkOutStr) {
        checkIn.clear();
        checkIn.sendKeys(checkInStr); //wait

        checkOut.clear();
        checkOut.sendKeys(checkOutStr);
    }

    public void setPeople(String adultCount, String childCount) {
        adultChild.click();
        adult.clear();
        adult.sendKeys(adultCount);
        child.clear();
        child.sendKeys(childCount);
        adultPlsButton.click();
    }
    public void performSearch() {
        buttonSearch.click();
    }







}
