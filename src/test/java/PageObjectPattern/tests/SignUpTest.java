package PageObjectPattern.tests;

import PageObjectPattern.Pages.ResultPage;
import PageObjectPattern.Pages.SignUpPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SignUpTest extends BaseTest {


    @Test
    public void signUpTestHappyPath() {
        SignUpPage signUpPage = new SignUpPage(driver);

        signUpPage.getVisibleMyAccountButton();
        signUpPage.getSignUpButton();
        signUpPage.sendFirstName("Krzysztof");
        signUpPage.sendLastName("Kowalski");
        signUpPage.sendMobileNumber("587444699");
        signUpPage.sendEmail();
        String passwordTest = "passqaz123";
        signUpPage.sendPassword(passwordTest);
        signUpPage.sendconfirmassword(passwordTest);

        signUpPage.getVisibleSubmitButton();
    }
    @Test
    public void  signUpTestLackOfAllParameters() {
        SignUpPage signUpPage = new SignUpPage(driver);

        signUpPage.getVisibleMyAccountButton();
        signUpPage.getSignUpButton();
        signUpPage.getVisibleSubmitButton();

        ResultPage resultPage = new ResultPage(driver);

        Assert.assertTrue(resultPage.getInfoLackOfEmail().isDisplayed());
        Assert.assertTrue(resultPage.getInfoLackOfPassword().isDisplayed());
        Assert.assertTrue(resultPage.getInfoLackOfPassword2().isDisplayed());
        Assert.assertTrue(resultPage.getInfoLackOfFirstName().isDisplayed());
        Assert.assertTrue(resultPage.getInfoLackOfLastName().isDisplayed());

        Assert.assertEquals(resultPage.getInfoLackOfEmail().getText(), "The Email field is required.");
        Assert.assertEquals(resultPage.getInfoLackOfPassword().getText(), "The Password field is required.");
        Assert.assertEquals(resultPage.getInfoLackOfPassword2().getText(), "The Password field is required.");
        Assert.assertEquals(resultPage.getInfoLackOfFirstName().getText(), "The First name field is required.");
        Assert.assertEquals(resultPage.getInfoLackOfLastName().getText(), "The Last Name field is required.");

    }

}
