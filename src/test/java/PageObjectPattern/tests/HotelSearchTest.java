package PageObjectPattern.tests;

import PageObjectPattern.Pages.HotelSearchPage;
import PageObjectPattern.Pages.ResultPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class HotelSearchTest extends BaseTest {

    @Test
    public void searcHoteltest() {
        //HotelSearchPage:
        HotelSearchPage hotelSearchPage = new HotelSearchPage(driver);

        hotelSearchPage.setCity("Dubai");
        hotelSearchPage.setDates("17/02/2023", "20/02/2023");
        hotelSearchPage.setPeople("2", "3");
        hotelSearchPage.performSearch();

        //ResultPage:
        ResultPage resultPage = new ResultPage(driver);

        List<String> allHotels = resultPage.getHotelNames();
        allHotels.forEach(el -> System.out.println(el));
        System.out.println("Liczba hoteli: " + allHotels.size());
        String hotel1 = "Jumeirah Beach Hotel";
        String hotel2 = "Oasis Beach Tower";
        String hotel3 = "Rose Rayhaan Rotana";
        String hotel4 = "Hyatt Regency Perth";
        Assert.assertEquals(allHotels.get(0), hotel1);
        Assert.assertEquals(allHotels.get(1), hotel2);
        Assert.assertEquals(allHotels.get(2), hotel3);
        Assert.assertEquals(allHotels.get(3), hotel4);
    }

    @Test void hotelSearchWithoutName() {
        HotelSearchPage hotelSearchPage = new HotelSearchPage(driver);
        ResultPage resultPage = new ResultPage(driver);

        //HotelSearchPage
        hotelSearchPage.setDates("17/02/2023", "20/02/2023");
        hotelSearchPage.setPeople("2", "3");
        hotelSearchPage.performSearch();

        //ResultPage
        WebElement result = resultPage.getResultHeading();
        Assert.assertTrue(result.isDisplayed());
        Assert.assertEquals(result.getText(), "No Results Found");
    }




}
