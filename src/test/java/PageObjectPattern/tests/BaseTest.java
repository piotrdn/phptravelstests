package PageObjectPattern.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    public WebDriver driver;
    @BeforeMethod
    public void beforeMethod() {
        //WebDriverManager.chromedriver().setup();
        //driver = new ChromeDriver();

        driver = Configuration.getDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://www.kurs-selenium.pl/demo/");
    }

//    @AfterMethod
//    public void afterMethod() {
//        driver.quit();
//    }


}
