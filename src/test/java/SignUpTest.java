import org.apache.hc.core5.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class SignUpTest extends BaseTest {

//    WebDriver driver;
//    @BeforeMethod
//    public void beforeMethod() {
//        driver = Configuration.getDriver();
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//    }
//    @AfterMethod
//    public void afterMethod() {
//        driver.quit();
//    }

    @Test
    public void firstTest() {
        //driver.get("http://www.kurs-selenium.pl/demo/");
        driver.findElements(By.xpath("//li[@id='li_myaccount']")) //są zwracane dwa identyczne elementy wiec nie da sie odwołac do pojedynczego findElement
                .stream()
                .filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);

        driver.findElements(By.xpath("//a[@class='go-text-right' and text()='  Sign Up']")) //też są zwaracne dwa elementy
                .stream()
                .filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);
        //driver.findElements(By.xpath("//a[@class='go-text-right' and text()='  Sign Up']")).get(1).click(); //alternatywne podejście ale nie jest one zawsze skuteczne bo może próbować klikać w niewidoczny element

        //Nowe okno SignUp
        WebElement firstName = driver.findElement(By.name("firstname"));
        firstName.click();
        firstName.clear();
        String firstNameStr = "Tomasz";
        firstName.sendKeys(firstNameStr);

        WebElement lastName = driver.findElement(By.name("lastname"));
        lastName.click();
        lastName.clear();
        lastName.sendKeys("ProblemXD");

        WebElement mobileNumber = driver.findElement(By.name("phone"));
        mobileNumber.click();
        mobileNumber.clear();
        mobileNumber.sendKeys("123456789");

        WebElement email = driver.findElement(By.name("email"));
        email.click();
        email.clear();
        String testEmail = "tomaszproblemXD"+ randomNumberForEmail() + "@gmail.com";
        email.sendKeys(testEmail); //email jest zapisywany w bazie więc żeby test zawsze przechodził należy generowac unikalne emaile

        WebElement password = driver.findElement(By.name("password"));
        password.click();
        password.clear();
        String passwordString = "superhaslo";
        password.sendKeys(passwordString);

        WebElement confirmPassword = driver.findElement(By.name("confirmpassword"));
        confirmPassword.click();
        confirmPassword.clear();
        confirmPassword.sendKeys(passwordString);

        //submit równiez występuje 2x (jeden widoczny, a drugi nie)
        driver.findElements(By.xpath("//div//button[@type='submit']")).stream().filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);




        //Nowa podstrona
        WebElement headerWithName = driver.findElement(By.xpath("//h3[@class='RTL']"));

        Assert.assertTrue(headerWithName.isDisplayed());
        Assert.assertTrue(headerWithName.getText().contains(firstNameStr));

    }

    public String randomNumberForEmail() {
        Random random = new Random();
        int temp = random.nextInt(1001); //zakres (0-1000)
        String result = Integer.toString(temp);
        return result;
    }
}
